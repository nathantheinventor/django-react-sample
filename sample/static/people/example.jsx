const Person = ({person}) => {
    return <div className="person">
        <h2>Person</h2>
        <span>{person.first_name} {person.last_name}</span>
    </div>
}

const People = ({people}) => {
    return <div>
        <h1>People</h1>
        {people.map(person => <Person person={person} key={person.id}/>)}
    </div>
}
