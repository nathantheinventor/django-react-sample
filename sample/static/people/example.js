const Person = ({
  person
}) => {
  return React.createElement("div", {
    className: "person"
  }, React.createElement("h2", null, "Person"), React.createElement("span", null, person.first_name, " ", person.last_name));
};

const People = ({
  people
}) => {
  return React.createElement("div", null, React.createElement("h1", null, "People"), people.map(person => React.createElement(Person, {
    person: person,
    key: person.id
  })));
};

