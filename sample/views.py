from django.shortcuts import render

from .models import Person


def people(request):
    people = Person.objects.all()
    context = {'people': people}
    return render(request, 'people/index.html', context)
