""" Urls for sample app """

from django.urls import path

from . import views

urlpatterns = [
    # ex: /polls/
    path('people/', views.people, name='people'),
]
